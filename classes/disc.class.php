<?php

class Disc extends Product {
    
    public $size;

    public function __construct($sku, $name, $price, $size){
       parent::__construct($sku, $name, $price);
       $this->size = $size;
    }

    public function insertDisc(){
        $str = "Size: {$this->size} MB";
        $insert = $this->insertData($this->sku, $this->name, $this->price, $str);
        return $insert;
    }

}