<?php

class Query extends Dbh {

    public function getProducts(){
        $sql = "SELECT * FROM products";
        $stmt = $this->connect()->query($sql);
        $rows = [];
        while($row = $stmt->fetch()){
            $rows[] = $row;
        }
        return $rows;
    }

    public function deleteData($id){
        $sql = "DELETE FROM products WHERE id = $id";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        return $stmt->rowCount();
    }

}