<?php

class Dbh {
    private $host = "localhost";
    private $user = "root";
    private $pwd = "";
    private $dbName = "test1";

    //for hosting : 
        //private $host = "localhost";
        //private $user = "id18530827_andy";
        //private $pwd = "~1]C=#2h2!jk<hyT";
        //private $dbName = "id18530827_scandiweb_v1";

    protected function connect(){
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName;
        $pdo = new PDO($dsn, $this->user, $this->pwd);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        return $pdo;
    }

}