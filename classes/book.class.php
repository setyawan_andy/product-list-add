<?php

class Book extends Product {
    
   public $weight;

   public function __construct($sku, $name, $price, $weight){
       parent::__construct($sku, $name, $price);
       $this->weight = $weight;
   }

   public function insertBook(){
        $str = "Weight: {$this->weight} KG";
        $insert = $this->insertData($this->sku, $this->name, $this->price, $str);
        return $insert;
    }
}