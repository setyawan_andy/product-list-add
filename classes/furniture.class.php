<?php

class Furniture extends Product {
    
    public  $height,
            $width,
            $length;

    public function __construct($sku, $name, $price, $height, $width, $length){
        parent::__construct($sku, $name, $price);
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }

    public function insertFurniture(){
        $str = "Dimension: {$this->height} x {$this->width} x {$this->length}";
        $insert = $this->insertData($this->sku, $this->name, $this->price, $str);
        return $insert;
    }

}