<?php

abstract class Product extends Dbh {
    protected   $sku,
                $name,
                $price;
            
    
    protected function __construct($sku, $name, $price) {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
    }

    protected function insertData($sku, $name, $price, $type){
        $sql = "INSERT INTO products(sku, name, price, type) VALUES (?, ?, ?, ?)";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute([$sku, $name, $price, $type]);

        return $stmt->rowCount();
    }

}