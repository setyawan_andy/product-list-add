<?php

session_start();

include 'includes/class-autoload.inc.php';
include "./classes/dbh.class.php";
include "./classes/query.class.php";



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="./assets/style.css">

    <title>Product List</title>
</head>
<body>

        <div class="container mt-5" id="container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container-fluid border-bottom border-2 border-dark pb-3">
                    <a class="fs-3 fw-bold navbar-brand" href="#">Product List</a>
                    <div class="d-flex">
                        <a href="addProduct.php"><button class="me-3 btn btn btn-outline-dark">ADD</button></a>
        <form action="includes/deleteproduct.inc.php" method="post">
                        <button class="btn btn-dark" type="submit" name="delete">MASS DELETE</button></a>
                    </div>
                </div>
            </nav>
        </div>

        <?php 
        
        $products = new Query;
        $productsObj = $products->getProducts();
        
        ?>

        <div class="container mt-5 px-4">
            <div class="row">
                <div class="col-6">
                    <?php
                    
                    if(isset($_SESSION['status']))
                    
                    { ?>
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                Product data is <strong> <?php echo $_SESSION['status']; ?> </strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    <?php 
                        unset($_SESSION['status']);
                    } ?>

                </div>
            </div>
            <div class="row gap-5 px-2">
           
                <?php foreach($productsObj as $product) : ?>
                <div class="col-2 border border-2 border-dark py-3 px-3">
                    <input class="delete-checkbox" name="checkbox[]" type="checkbox" value="<?php echo $product['id']; ?>">
                    <div class="d-flex flex-column justify-content-center text-center">
                        <p><?php echo $product['sku']; ?></p>
                        <p><?php echo $product['name']; ?></p>
                        <p><?php echo $product['price']; ?>.00 $</p>
                        <p><?php echo $product['type']; ?></p>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </form>

    <div class="container border-top border-2 border-dark bg-white fixed-bottom">
        <footer class="text-center py-3">
            <span class="text-muted">Scandiweb Test Assignment</span>
        </footer>
    </div>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>
</html>