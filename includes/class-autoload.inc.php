<?php

spl_autoload_register('myAutoLoader');

function myAutoLoader($className){
    
    if( !session_id() ) session_start();

    $path = "classes/";
    $ext = ".class.php";
    $fullPath = $path . $className . $ext;

    include_once $fullPath;
}