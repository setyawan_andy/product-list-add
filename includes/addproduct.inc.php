<?php

session_start();

if(isset($_POST['submit'])){
    
    $sku = $_POST['sku'];
    $name = $_POST['name'];
    $price = $_POST['price'];
    $type = $_POST['type'];
    $size = $_POST['size'];
    $height = $_POST['height'];
    $width = $_POST['width'];
    $length = $_POST['length'];
    $weight = $_POST['weight'];

    include "../classes/dbh.class.php";
    include "../classes/query.class.php";
    include "../classes/product.class.php";
    include "../classes/disc.class.php";
    include "../classes/furniture.class.php";
    include "../classes/book.class.php";

    $insertData;

    switch ($type) {
        case 'dvd':
            $disc = new Disc($sku, $name, $price, $size);
            $insertData = $disc->insertDisc();
            break;
        case 'furniture':
            $furniture = new Furniture($sku, $name, $price, $height, $width, $length);
            $insertData = $furniture->insertFurniture();
            break;
        case 'book':
            $book = new Book($sku, $name, $price, $weight);
            $insertData = $book->insertBook();
            break;
        default:
            break;
    }

    

    if($insertData > 0){

        $_SESSION['status'] = "successfully saved !";

        header('location: ../index.php');
        
    } else {
        $_SESSION['status'] = "failed to be saved !";
        header('location: ../index.php');
    }
}