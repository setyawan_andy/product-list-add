<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/style.css">

    <title>Add Product</title>
</head>
<body>

    <div class="container mt-5">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container-fluid border-bottom border-2 border-dark pb-3">
                <a class="fs-3 fw-bold navbar-brand" href="#">Product Add</a>
                <div class="d-flex flex-row-reverse">
                <a href="index.php"><button name="cancel" class="btn btn-dark">Cancel</button></a>
    
    <form action="includes/addproduct.inc.php" method="post" id="product_form">                
                    
                <button type="submit" name="submit" class="me-3 btn btn btn-outline-dark">Save</button>

                </div>
            </div>
        </nav>
    </div>

    <div class="container mt-5 px-4">
        

            <div class="row mb-3">
                <label for="sku" class="py-2 col-2">SKU</label>
                <div class="col-3">
                    <input type="text" class="form-control border-dark" id="sku" name="sku" autocomplete="off" required>
                </div>
            </div>
            <div class="row mb-3">
                <label for="name" class="py-2 col-2">Name</label>
                <div class="col-3">
                    <input type="text" class="form-control border-dark" id="name" name="name" autocomplete="off" required>
                </div>
            </div>
            <div class="row mb-3">
                <label for="price" class="py-2 col-2">Price ($)</label>
                <div class="col-3">
                    <input type="number" class="form-control border-dark" id="price" name="price" autocomplete="off" required>
                </div>
            </div>
            
            <div>
                <div class="row mb-3">
                    <label for="type" class="py-2 col-2">Type</label>
                    <div class="col-3">
                        <select class="form-select border-dark" id="productType" name="type">
                            <option value="dvd">DVD</option>
                            <option value="furniture">Furniture</option>
                            <option value="book">Book</option>
                        </select>
                    </div>
                </div>

                <!-- DVD -->
                <div id="dvd" class="data">
                    <div class="row mb-3">
                        <label for="size" class="py-2 col-2">Size (MB)</label>
                        <div class="col-3">
                            <input type="number" class="form-control border-dark" id="size" name="size" autocomplete="off">
                        </div>
                    </div>
                    <p class="fw-bold">*Please provide size in MB format</p>
                </div>
                

                <!-- Furniture -->
                <div id="furniture" class="data">
                    <div class="row mb-3">
                        <label for="height" class="py-2 col-2">Height (CM)</label>
                        <div class="col-3">
                            <input type="number" class="form-control border-dark" id="height" name="height" autocomplete="off" >
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="width" class="py-2 col-2">Width (CM)</label>
                        <div class="col-3">
                            <input type="number" class="form-control border-dark" id="width" name="width" autocomplete="off" >
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="length" class="py-2 col-2">Length (CM)</label>
                        <div class="col-3">
                            <input type="number" class="form-control border-dark" id="length" name="length" autocomplete="off" >
                        </div>
                    </div>
                    <p class="fw-bold">*Please provide dimensions in HxWxL(CM) format</p>
                </div>
                
                <!-- Book -->
                <div id="book" class="data">
                    <div class="row mb-3">
                        <label for="weight" class="py-2 col-2">Weight (KG)</label>
                        <div class="col-3">
                            <input type="number" class="form-control border-dark" id="weight" name="weight" autocomplete="off" >
                        </div>
                    </div>
                    <p class="fw-bold">*Please provide weight in KG format</p>
                </div>
            </div>
            
    </div>
</form>

    <div class="container border-top border-2 border-dark bg-white fixed-bottom">
        <footer class="text-center py-3">
            <span class="text-muted">Scandiweb Test Assignment</span>
        </footer>
    </div>
    

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $("#productType").on('change', function() {
            $(".data").hide();
            $("#" + $(this).val()).fadeIn(300);
        }).change();
    });
    $(document).ready(function(){
        $("#productType").on('change', function(){
            $(".data input").attr('required', false);
            $("#" + $(this).val() + " input").attr('required', true);
        }).change();
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>
</html>